## Alunos

- Victor Matias Navarro 14/0032932
- Vitor Barbosa de Araújo 14/0033149

## Ferramentas Utilizadas
* Linux Mint 18 Sonya
* Vim
* Pacotes:
  * isc-dhcp-server
  * iptables
  * wondershaper
* Cabo de rede
* _Script_ _shell_ para configurar as tabelas de IP

## Procedimento

Antes de comentar acerca da implementação do _firewall_, é importante notar que
a estrutura construída para o segundo trabalho prático da disciplina foi utilizada aqui. Dessa forma, a documentação daquele trabalho é válida como prévia para este e será mandada como um adicional na entrega.

Para a realização deste trabalho, foi utilizada a seguinte topologia de rede:

![Topologia da _rede_](rede_topology.png)

Um computador atua como roteador e servidor para o cliente, que se conecta ao mesmo pela interface de entrada enp1s0. O servidor comunica-se com a _internet_ com uma conexão _wireless_ através da interface wlp2s0.

Com esse conhecimento, pode-se explicar a implementação das duas partes do trabalho.

### _Firewall_

O _firewall_ implementado não foi realizado em modo _bridge_. Foi utilizado o iptables para configurar as regras que basicamente cumprem os seguintes objetivos:

* Permitir passagem de tráfego HTTP e HTTPS
* Permitir passagem de trafégo DNS
* Permitir passagem de tráfego ICMP

Foi criado um _script_ em _shell_ para facilitar a criação das regras. O conteúdo do mesmo é:

```shell
iptables -F
iptables -X
iptables -A FORWARD -i enp1s0 -o wlp2s0 -p udp -m udp --dport 53 -j ACCEPT
iptables -A FORWARD -i enp1s0 -o wlp2s0 -p tcp -m tcp --dport 80 -j ACCEPT
iptables -A FORWARD -i enp1s0 -o wlp2s0 -p tcp -m tcp --dport 443 -j ACCEPT
iptables -A FORWARD -i enp1s0 -o wlp2s0 -p icmp -j ACCEPT
iptables -A FORWARD -i enp1s0 -o wlp2s0 -j DROP
```

Para ficar mais didático, será explicado as linhas separadamente:

```shell
iptables -F
iptables -X
```
Essa linha apaga todas as regras atuais da tabela, incluindo as criadas pelo usuário. É uma limpeza antes de inserir as próximas regras. Um entendimento fundamental que deve-se ter é que as regras são lidas de cima para baixo. Então, as regras de permissão de acesso devem estar em cima e as de negação de acesso devem estar embaixo.

```shell
iptables -A FORWARD -i enp1s0 -o wlp2s0 -p udp -m udp --dport 53 -j ACCEPT
iptables -A FORWARD -i enp1s0 -o wlp2s0 -p tcp -m tcp --dport 80 -j ACCEPT
iptables -A FORWARD -i enp1s0 -o wlp2s0 -p tcp -m tcp --dport 443 -j ACCEPT
iptables -A FORWARD -i enp1s0 -o wlp2s0 -p icmp -j ACCEPT
iptables -A FORWARD -i enp1s0 -o wlp2s0 -j DROP
```

Essas são as regras que filtram os pacotes. Elas atuam na interface à qual os clientes se conectam, ou seja, a enp1s0. Cada regra especifica o redirecionamento (_forward_) da interface enp1s0 para a interface de saída wlp2s0 se, e somente se, os próximos argumentos sejam cumpridos.

Na primeira linha, por exemplo, permite-se o acesso (através da ação ACCEPT) de pacotes que utilizem o protocolo UDP e que tenham como porta de destino a porta de número 53, que é utilizada pra DNS.

As próximas duas linhas especificam a aceitação de pacotes que utilizam o protocolo TCP e tenham como portas de destino as portas de número 80 e 443, HTTP e HTTPS respectivamente. Por fim, a última linha permite o tráfego de pacotes do tipo ICMP, para _ping_ e outras informações úteis.

A última linha é a parte interessante do _firewall_. Para cada pacote, será analisado se ele atende os requisitos de cada regra. Se não atender, ele passa sempre para a regra de baixo (observe como as regras são inseridas com a opção -A, que significa "append"). Se ele não der _match_ em nenhuma regra de aceitação, ele cai na última regra que simplesmente descarta o pacote. E então, o controle de tráfego é feito.


### _QoS_

Com o _firewall_, sobra a parte de limitar a banda da interface. Para isso, foi necessário a instalação do pacote _wondershaper_.

```shell
sudo apt-get install wondershaper
```

O _wondershaper_ é uma aplicação que serve justamente para realizar o modelamento de largura de banda. Com o pacote instalado, deve-se respeitar a seguinte sintaxe:

```shell
sudo wondershaper interface _download-speed_ _upload-speed_
```

No caso da atividade prática, deveria-se limitar a banda à 1Mbps. Um fator importante a ser notado é que o comando do wondershaper utiliza a velocidade em _kilobits_, sendo necessária uma pequena conversão. Para atingir ao objetivo do trabalho, o comando utilizado foi:

```shell
sudo wondershaper enp1s0 1000 1000
```

O comando limita o tráfego da interface enp1s0 (pela qual os clientes se conectam a _internet_) a 1000 Kbps (que é equivalente à 1Mbps).
