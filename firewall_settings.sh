iptables -F
iptables -X
iptables -A FORWARD -i enp1s0 -o wlp2s0 -p udp -m udp --dport 53 -j ACCEPT
iptables -A FORWARD -i enp1s0 -o wlp2s0 -p tcp -m tcp --dport 80 -j ACCEPT
iptables -A FORWARD -i enp1s0 -o wlp2s0 -p tcp -m tcp --dport 443 -j ACCEPT
iptables -A FORWARD -i enp1s0 -o wlp2s0 -p icmp -j ACCEPT
iptables -A FORWARD -i enp1s0 -o wlp2s0 -j DROP
